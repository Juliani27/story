from django import forms
from .models import Schedule
import datetime

class Schedule_Form(forms.ModelForm):
	class Meta: 
		model = Schedule
		fields = ('name', 'day', 'date', 'time', 'place', 'category')
		fields_required = ('name', 'day', 'date', 'time', 'place', 'category')
		widgets = {
			'name': forms.TextInput(attrs={'class' : 'form-control', 'placeholder': 'Activity name'}),
			'day' : forms.Select(attrs={'class' : 'form-control'}),
			'date' : forms.DateInput(attrs={'type': 'date', 'class': 'form-control'}),
			'time' : forms.TimeInput(attrs={'type' : 'time', 'class' : 'form-control'}),
			'place' : forms.TextInput(attrs={'class' : 'form-control', 'placeholder': 'Activity place'}),
			'category': forms.TextInput(attrs={'class' : 'form-control', 'placeholder': 'Activity category'})
		}

	def clean_time(self):
		day = self.cleaned_data['day']
		date = self.cleaned_data['date']
		time = self.cleaned_data['time']
		date_time = ('%s %s' % (date, time))
		date_time = datetime.datetime.strptime(date_time, '%Y-%m-%d %H:%M:%S')
		date_day = date_time.strftime("%A")
		if datetime.datetime.now() >= date_time:
			self.add_error('date', "Wrong Date or Time!")
		if day != date_day:
			self.add_error('day', "Wrong Day!")
		return time
