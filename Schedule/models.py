from django.db import models

class Schedule(models.Model):
	DAYS = (
		('Sunday', 'Sunday'),
		('Monday', 'Monday'),
		('Tuesday', 'Tuesday'),
		('Wednesday', 'Wednesday'),
		('Thursday', 'Thursday'),
		('Friday', 'Friday'),
		('Saturday', 'Saturday')
	)
	name = models.CharField(max_length = 20, )
	day = models.CharField(max_length = 10, choices=DAYS)
	date = models.DateField()
	time = models.TimeField()
	place = models.CharField(max_length = 20)
	category = models.CharField(max_length = 15)

