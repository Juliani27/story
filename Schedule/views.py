from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Schedule
from .forms import Schedule_Form
from .models import Schedule

response = {}

def index(request):
	schedule = Schedule.objects.all().order_by('date', 'time')
	response['schedule'] = schedule
	return render(request, 'MySchedule.html', response)

def AddSchedule(request):
	response['form'] = Schedule_Form
	return render(request, 'Schedule.html', response)

def saveSchedule(request):
	form = Schedule_Form(request.POST or None)
	if (request.method == "POST" and form.is_valid()):
		response['name'] = request.POST['name']
		response['day'] = request.POST['day']
		response['date'] = request.POST['date']
		response['time'] = request.POST['time']
		response['place'] = request.POST['place']
		response['category'] = request.POST['category']
		schedule = Schedule(name=response['name'], day=response['day'], date=response['date'],
			time=response['time'], place=response['place'], category=response['category'])
		schedule.save()
		return HttpResponseRedirect('/../Schedule/')
	
	return render(request, 'Schedule.html', {'form':form})

def deleteAll(request):
	response['schedule'] = Schedule.objects.all().delete()
	response.clear()
	return HttpResponseRedirect('/../Schedule/')