from django.urls import path
from . import views

app_name = 'Schedule'
urlpatterns = [
	path('', views.index, name='index'),
	path('AddSchedule/', views.AddSchedule, name='AddSchedule'),
    path('saveSchedule/', views.saveSchedule, name='saveSchedule'),
    path('deleteAll/', views.deleteAll, name='deleteAll')
]
